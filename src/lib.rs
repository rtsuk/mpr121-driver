#![no_std]

use embedded_hal::blocking::i2c::{WriteRead, Write};

pub enum Error<I2cError> {
    I2cError(I2cError),
}

pub struct Mpr121Driver<I2C> {
    i2c: I2C,
    addr: u8,
}

impl<I2C, E> Mpr121Driver<I2C>
where
    I2C: WriteRead<Error = E> + Write<Error = E>,
{
    pub fn new(i2c: I2C, addr: chip::AddrPinConnectedTo) -> Result<Mpr121Driver<I2C>, Error<E>> {
        Ok(Self {
            i2c: i2c,
            addr: match addr {
                chip::AddrPinConnectedTo::Vss => chip::addr::VSS_ADDR,
                chip::AddrPinConnectedTo::Vdd => chip::addr::VDD_ADDR,
                chip::AddrPinConnectedTo::Sda => chip::addr::SDA_ADDR,
                chip::AddrPinConnectedTo::Scl => chip::addr::SCL_ADDR,
            },
        })
    }

    pub fn enable_electrodes(&mut self, ele_en: u8) -> Result<(), E> {
        let mut buffer = [0u8; 1];
        self.i2c.write_read(self.addr, &[register::addr::ECR_ADDR], &mut buffer)?;
        let old: u8 = buffer[0];
        let new: u8 = (old & 0b11110000) | (ele_en & 0b00001111);
        let bytes = [register::addr::ECR_ADDR, new];
        self.i2c.write(self.addr, &bytes)?;

        Ok(())
    }
}

mod chip {
    pub enum AddrPinConnectedTo {
        Vss,
        Vdd,
        Sda,
        Scl,
    }

    pub mod addr {
        pub const VSS_ADDR: u8 = 0x5A;
        pub const VDD_ADDR: u8 = 0x5B;
        pub const SDA_ADDR: u8 = 0x5C;
        pub const SCL_ADDR: u8 = 0x5D;
    }
}

mod register {
    pub mod addr {
        pub const ECR_ADDR: u8 = 0x5E;
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn test_it() {
       unimplemented!()
    }
}
